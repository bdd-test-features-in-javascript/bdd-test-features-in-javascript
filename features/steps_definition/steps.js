const { Given, When, Then } = require('@cucumber/cucumber')
const { assertThat, is } = require('hamjest')

const { Person, Network } = require('../../support/person_parameter')

Given('a person named {word}', function (name) {
  this.network = new Network()
  this.people = {}
  this.people[name] = new Person(this.network)
})

When('Sean shouts {string}', function (message) {
  this.people['Sean'].shout(message)
  this.messageFromSean = message
})

Then('Lucy should hear Sean\'s message', function () {
  assertThat(this.people['Lucy'].messagesHeard(), is([this.messageFromSean]))
})
